# Cha Cha Parrot
Cha Cha Parrot is a SHA-1 hash brute forcer. It's not optimized like other hashing tools (hashcat, JTR), and was created to attempt brute force operations against known SHA-1 computed hashes.

System requirements
---

This project requires the Rust compiler and (optionally) Podman.

```
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh

source $HOME/.cargo/env
rustc -h

sudo pacman -S podman
podman run -ti debian:latest
```

Running
---

Obtain a wordlist online and replace the contents of `wordlist.txt` or feel free to use the one included, and proceed to throw a SHA-1 at it. This example uses the password `nutritional`:

```
cargo run -- wordlist.txt 6b3fe53e13dcafb0876972ff6039d3670f1760c5
```
