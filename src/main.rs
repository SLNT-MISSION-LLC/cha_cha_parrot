use sha1::Digest;
use std::{
    env,
    error::Error,
    fs::File,
    io::{BufRead, BufReader},
};

const SHA1_HEX_STRING_LENGTH: usize = 40;

fn main() -> Result<(), Box<dyn Error>> {
    let args: Vec<String> = env::args().collect();

    if args.len() != 3 {
        println!("Usage:");
        println!("cha_cha_parrot: <wordlist.txt> <SHA-1 Hash>");
        return Ok(());
    }

    let hash = args[2].trim();
    if hash.len() != SHA1_HEX_STRING_LENGTH {
        return Err("not a valid SHA-1 hash".into());
    }
    // Source for wordlist - https://www.mit.edu/~ecprice/wordlist.10000
    // GCHQ's Cyber Chef was used to compute SHA-1's
    // https://gchq.github.io/CyberChef/#recipe=SHA1(80)&input=bnV0cml0aW9uYWw
    let wordlist_file = File::open(&args[1])?;
    let reader = BufReader::new(&wordlist_file);

    for line in reader.lines() {
        let line = line?;
        let common_password = line.trim();

        if hash == &hex::encode(sha1::Sha1::digest(common_password.as_bytes())) {
            println!("Password found: {}", &common_password);
            return Ok(());
        }
    }
    println!("Password was not found in our wordlist");

    Ok(())
}
